#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
int main(int argc, char* argv[], char* envp[])
{
	  char* path = argv[1];
	  pid_t  id = fork(); //child_PID / Zero
            if(id <0)
	    {
		    perror("fork() error!");
		    exit(-1);
				    
	    }
         else if (id !=0)
	 {
	 printf("I am a parent process! \n my_id: %d , mychild_id: %d \n",getpid(),id);
	 wait(0); //wait for the child process 
	 }
	else 
	{
	printf("I am a child process! \n my_id: %d, myParent_id: %d \n",getpid(),getppid());
	execv(path,argv+1);
	}
	    
	      return 0;
}

